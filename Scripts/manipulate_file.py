import re
import nltk
import numpy as np
from nltk.corpus import stopwords
# splits file in 2 lists and strips the first word
def categorize_msg(file):
    stop_words = set(stopwords.words('english'))
    spam_list = []
    ham_list = []
    with open(file, 'r') as file:
        for line in file:
            msg = line.split(None, 1)[0]
            line = line.split(msg, 1)[1]
            line = re.sub(r'[^\w\s]','',line)
            tokens = nltk.word_tokenize(line)
            for w in tokens:
                if w.lower() in stop_words:
                    line = line.replace(w, '')
            if msg == "spam":
                spam_list.append(line)
            else:
                ham_list.append(line)
    return spam_list, ham_list

def get_data(file):
    np_array = np.empty(shape=[0, 2])
    array = []
    with open(file, 'r') as file:
        for line in file:
            line = re.sub('[\]\[]', '', line)
            points = line.split(',')
            array.append([float(points[0]), float(points[1])])
        np_array = np.asarray(array)
    return np_array

def test_data(file):
    stop_words = set(stopwords.words('english'))
    line_list = []
    with open(file, 'r') as file:
        for line in file:
            line = re.sub(r'[^\w\s]', '', line)
            tokens = nltk.word_tokenize(line)
            for w in tokens:
                if w.lower() in stop_words:
                    line = line.replace(w, '')
            line_list.append(line)
    return line_list

# spam, ham = categorize_msg('../Inputs/test_corpus.txt')
# print(len(spam))
# spam = 747
# ham = 4827
