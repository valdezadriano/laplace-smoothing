import random
from collections import Counter

from manipulate_file import categorize_msg, test_data


# A = prob that msg is spam
# B = content of msg
# if P(A|B) > P(!A|B) then msg is spam
# applying Bayes' theorem
# P(A)*P(B|A) > P(!A)*P(B|!A), both are divided by P(B) so we can eliminate it
# P(A) and P(!A) are percentage of spam and ham


def partition_sets(msgs):
    ten_per = int(round(0.1 * len(spam)))
    shuffled_list = msgs[:]
    random.shuffle(shuffled_list)
    list_training = shuffled_list[ten_per + ten_per:]
    list_validation = shuffled_list[:ten_per]
    list_test = shuffled_list[ten_per:ten_per + ten_per]
    return list_training, list_validation, list_test


def percentage_spam(spam_list, ham_list):
    total = float(len(ham_list) + len(spam_list))
    perc_spam = len(spam_list) / total
    perc_ham = len(ham_list) / total
    return perc_spam, perc_ham


def count_words():
    cnt_spam = Counter()
    cnt_ham = Counter()
    for msg in spam_training:
        for word_ in msg.split():
            cnt_spam[word_] += 1
    for msg in ham_training:
        for word_ in msg.split():
            cnt_ham[word_] += 1
    return cnt_spam, cnt_ham


def conditional_word(word_, counter, k, voc_size):
    return (counter[word_] + k) / float(sum(counter.values()) + k * voc_size)


def conditional_msg(msg, counter, k, voc_size):
    total = 1.0
    for word_ in msg.split():
        total *= conditional_word(word_, counter, k, voc_size)
    return total


def classifier(msg, spam_training_set, ham_training_set, k, voc_size):
    cnt_spam, cnt_ham = count_words()
    perc_spam, perc_ham = percentage_spam(spam_training_set, ham_training_set)
    is_spam_ = perc_spam * conditional_msg(msg, cnt_spam, k, voc_size)
    is_ham = perc_ham * conditional_msg(msg, cnt_ham, k, voc_size)
    return is_spam_ > is_ham


spam, ham = categorize_msg('../Inputs/test_corpus.txt')

spam_training, spam_validation, spam_test = partition_sets(spam)
ham_training, ham_validation, ham_test = partition_sets(ham)

spam_w, ham_w = count_words()
voc_list = list(set(spam_w + ham_w))
voc_len = len(voc_list)

validation_set = list(set(spam_validation + ham_validation))
test_set = list(set(spam_test + ham_test))

test_set = test_data('../Inputs/test_sms.txt')

unk_counter = 1
for line in validation_set:
    for word in line.split():
        if word not in voc_list:
            unk_counter = unk_counter + 1

file = open('../Outputs/email_classifier.txt', 'w')

for line in test_set:
    is_spam = classifier(line, spam_training, ham_training, (unk_counter / voc_len), voc_len)
    line = '{}{}{}'.format(is_spam, '\t', line)
    file.write(line)
file.close()
