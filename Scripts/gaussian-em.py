import random

import numpy as np
import matplotlib.pyplot as plt
from manipulate_file import get_data
from scipy.stats import multivariate_normal
import copy
from matplotlib.patches import Ellipse


def gaussian_distribution(sigma_i, mu_i, vector_i):
    # fraction = np.sqrt(2*np.pi*np.linalg.det(sigma_i))
    # exponent = np.subtract(vector_i, mu_i).T'

    normal = multivariate_normal.pdf(x=vector_i, mean=mu_i, cov=sigma_i, allow_singular=True)
    return normal


def gamma_total(sigma_i, mu_i, pi_i):
    return sum([update_prob(sigma_i, mu_i, point, pi_i) for point in data])


def update_prob(sigma_i, mu_i, x_x, pi_i):
    n = gaussian_distribution(sigma_i, mu_i, x_x)
    e = (n * pi_i)
    r = sum([(gaussian_distribution(sigma[w], mu[w], x_x) * pi[w])
             for w in range(0, gaussians)])
    prob = e
    if r >= 0:
        prob = e/r
    return prob


def update_sigma(gamma, sigma_i, mu_i, pi_i):
     # numerator = np.zeros([2, 2])
     # for point in data:
     #   numerator = (np.multiply(np.dot((point - mu_i).T, (point - mu_i)), update_prob(sigma_i, mu_i, point, pi_i))) \
     #               + numerator
     # return numerator / gamma
     return np.cov(np.append(data, [mu_i], axis=0).T)


def update_mu(gamma, sigma_i, mu_i, pi_i):
    numerator = 0
    for point in data:
        numerator = np.dot(update_prob(sigma_i, mu_i, point, pi_i), point) + numerator
    return numerator / gamma


def update_pi(gamma):
    return gamma / len(data)


def expectation():
    cluster_expectancy = []
    for point in data:
        expectancies_ = [update_prob(sigma[w], mu[w], point, pi[w]) for w in range(gaussians)]
        index_ = expectancies_.index(max(expectancies_))
        cluster_expectancy.append(index_)
    return cluster_expectancy


def eigenso(covariance_):
    values_, vectors_ = np.linalg.eigh(covariance_)
    order = values_.argsort()[::-1]
    return values_[order], vectors_[:, order]


data = get_data('../Inputs/test_gmm_4.txt')
data_T = data.T

gaussians = int(input("Clusters? "))
mu = np.random.uniform(0, 9, size=(gaussians, 2))

for k in range(gaussians):
    mu[k] = data[random.randint(0, len(data))]

sigma = []
for x in range(0, gaussians):
    sigma.append(np.cov(data_T))

pi = [1. / gaussians] * gaussians

iterations = 0
while iterations < 100:
    for i in range(0, gaussians):
        # gamma = update_prob(sigma[i], mu[i], x, pi[i])
        # E step
        gamma_tot = gamma_total(sigma[i], mu[i], pi[i]) #N_sub_k
        actual_mu = copy.copy(mu[i])
        actual_pi = copy.copy(pi[i])
        actual_sigma = copy.copy(sigma[i])
        # M step
        mu[i] = update_mu(gamma_tot, actual_sigma, actual_mu, actual_pi)
        sigma[i] = update_sigma(gamma_tot, actual_sigma, mu[i], actual_pi)
        pi[i] = update_pi(gamma_tot)
    iterations += 1
    #print(iterations)

print(mu)

fig, ax = plt.subplots()

probs = expectation()
colors = "bgrcmykw"

for i in range(len(data)):
    [xp, yp] = data[i]
    ax.scatter(xp, yp, color=colors[probs[i]])

for i in range(0, gaussians):
    [xm, ym] = mu[i]
    covariance = sigma[i]
    values, vectors = eigenso(covariance)
    th = np.degrees(np.arctan2(*vectors[:, 0][::-1]))
    for j in range(0, 5):
        width, height = j * np.sqrt(values)
        elipse = Ellipse(xy=(xm, ym), width=width, height=height, angle=th, color=colors[i])
        elipse.set_facecolor('none')
        ax.add_artist(elipse)
    ax.scatter(xm, ym, marker='^', color=colors[i])

plt.show()

ex = input("quit to exit, point as \'x,y\'")
while ex != "quit":
    [x, y] = ex.split(',')
    new_point = np.array([[x, y]])
    expectancies = [update_prob(sigma[k], mu[k], new_point, pi[k]) for k in range(gaussians)]
    index = expectancies.index(max(expectancies))
    print("Belongs to cluster ", index)
    print(expectancies)
    ex = input("quit to exit, point as \'x,y\'")
